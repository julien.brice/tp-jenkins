import java.util.Scanner;

public class JeuMorpion {
	
	//Attributs
	public Grille grille;
	public Coordonnées2D coordonnées;
	
	//Constructeur
	public JeuMorpion() {
		grille = new Grille();
		coordonnées = new Coordonnées2D(5,5);
	}
	
	//Méthode tour de jeu
	public void tourDeJeu(String joueur) {
		System.out.println("C'est au tour de Joueur " + joueur);
		this.coordonnées = coordonnées.saisieCoord();
		if (this.grille.grille[coordonnées.x-1][coordonnées.y-1] == "*") {
			grille.remplirGrille(coordonnées, joueur);
		} else {
			System.out.println("Case déjà remplie");
			this.tourDeJeu(joueur);
		}
		grille.afficherGrille();		
	}
	
	//Méthode jouer
	public void jouer() {
		System.out.println("Bienvenue dans ce sublime jeu de morpion, un joueur prend les x, et un autre o");
		System.out.println("Le joueur ayant les x commence");
		grille.initGrille();
		grille.afficherGrille();
		int i = 0;
		boolean victoire = false;
		while(i<9 && !victoire) {
			if(i % 2 == 0) {
				this.tourDeJeu("x");
				i ++;
				victoire = grille.testVictoire();
			} else {
				this.tourDeJeu("o");
				i ++;
				victoire = grille.testVictoire();
			}
		}
		grille.afficherGrille();
	}

}
