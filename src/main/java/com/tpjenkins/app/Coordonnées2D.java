import java.util.Scanner;

public class Coordonnées2D {
	
	//Attributs
	public int x;
	public int y;
	
	//Méthodes
	
	//Constructeur
	public Coordonnées2D(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	//Ajouter
	public void ajouter(Coordonnées2D c) {
		this.x += c.x;
		this.y += c.y;
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

	public Coordonnées2D saisieCoord() {
		System.out.println("Sur quelle case voulez vous jouer ? Abscisse d'abord ;)");
		Scanner in = new Scanner(System.in);
		this.y = in.nextInt();
		System.out.println("Ordonnée maintenant !");
		this.x = in.nextInt();
		return this;
	}

}
