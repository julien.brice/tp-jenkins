import java.util.Scanner;

public class Grille {
	
	//Attributs
	public String[][] grille;
	public String joueur;
	
	//Constructeur
	
	public Grille() {
		this.grille = new String[3][3];
	}
	
	//Initialisation de la grille
	
	public void initGrille() {
		for(int i = 0; i<3; i ++) {
			for(int j = 0; j<3; j++) {
				grille[i][j] = "*";
			}
		}
	}
	
	//Méthode pour remplir la grille
	
	public void remplirGrille(Coordonnées2D coordonnées, String joueur) {
		this.grille[coordonnées.x-1][coordonnées.y-1] = joueur;
	}
	
	//Affichage de la grille
	
	public void afficherGrille() {
		for(int i = 0; i < 5; i++) {
			String ligne = "";
			for(int j = 0; j < 5; j++) {
				if(i == 0 || i == 4 || j == 0 || j == 4) {
					ligne += ".";
				} else {
					ligne += this.grille[i-1][j-1];
				}
			}
			System.out.println(ligne);
		}
	}
	
	//Méthode Test de victoire
	
	public boolean testVictoire() {
		boolean victoire = false;
		if(this.grille[0][0] == this.grille[0][1] && this.grille[0][1] == this.grille[0][2] && this.grille[0][0] != "*") {
			System.out.println("Bravo le joueur " + this.grille[0][1] + " a gagné !!");
			victoire = true;
		} else if (this.grille[1][0] == this.grille[1][1] && this.grille[1][1] == this.grille[1][2] && this.grille[0][2] != "*") {
			System.out.println("Bravo le joueur " + this.grille[1][1] + " a gagné !!");
			victoire = true;
		} else if (this.grille[2][0] == this.grille[2][1] && this.grille[2][1] == this.grille[2][2] && this.grille[2][2] != "*") {
			System.out.println("Bravo le joueur " + this.grille[2][1] + " a gagné !!");
			victoire = true;
		} else if (this.grille[0][0] == this.grille[1][0] && this.grille[1][0] == this.grille[2][0] && this.grille[2][0] != "*") {
			System.out.println("Bravo le joueur " + this.grille[0][0] + " a gagné !!");
			victoire = true;
		} else if (this.grille[0][1] == this.grille[1][1] && this.grille[1][1] == this.grille[2][1] && this.grille[2][1] != "*") {
			System.out.println("Bravo le joueur " + this.grille[0][1] + " a gagné !!");
			victoire = true;
		} else if (this.grille[0][2] == this.grille[1][2] && this.grille[1][2] == this.grille[2][2] && this.grille[2][2] != "*") {
			System.out.println("Bravo le joueur " + this.grille[0][2] + " a gagné !!");
			victoire = true;
		} else if (this.grille[0][0] == this.grille[1][1] && this.grille[1][1] == this.grille[2][2] && this.grille[2][2] != "*") {
			System.out.println("Bravo le joueur " + this.grille[0][0] + " a gagné !!");
			victoire = true;
		} else if (this.grille[2][0] == this.grille[1][1] && this.grille[1][1] == this.grille[0][2] && this.grille[1][1] != "*") {
			System.out.println("Bravo le joueur " + this.grille[1][1] + " a gagné !!");
			victoire = true;
		} else {
			System.out.println("Dommage, personne n'a gagné :(");
		}
		return victoire;
	}

}
